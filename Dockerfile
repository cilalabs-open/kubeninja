FROM bitnami/minideb

RUN install_packages curl ca-certificates python && \
    # kubectl installation
    curl -L -k -o /usr/local/bin/kubectl https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl   && \
    chmod +x /usr/local/bin/kubectl && \ 
    # sops installation
    curl -L -k https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux -o /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops && \
    # mongosh installation
    curl -L -k https://downloads.mongodb.com/compass/mongodb-mongosh_1.1.9_amd64.deb -o /usr/local/bin/mongoshell && \
    dpkg -x /usr/local/bin/mongoshell /usr/local/bin && \
    mv /usr/local/bin/usr/bin/mongosh /usr/local/bin && \
    rm -rf /usr/local/bin/mongoshell /usr/local/bin/usr && \
    chmod +x /usr/local/bin/mongosh && \
    # gcloud installation 
    curl -L -k https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-369.0.0-linux-x86_64.tar.gz -o /usr/local/bin/googlecli && \
    tar -C /usr/local/bin -xvzf /usr/local/bin/googlecli && \
    mv /usr/local/bin/google-cloud-sdk/bin/* /usr/local/bin && mv /usr/local/bin/google-cloud-sdk/lib/* /usr/local/lib && \
    rm -rf /usr/local/bin/googlecli /usr/local/bin/anthoscli && \
    apt-get purge -y curl

COPY deploy.sh /usr/local/bin/deploy

RUN chmod +x /usr/local/bin/deploy 

ENTRYPOINT ["/bin/bash", "-c"]



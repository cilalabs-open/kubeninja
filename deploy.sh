#!/bin/bash

case $1 in
     "frontend")
        kubectl version
        kubectl create namespace $KUBE_NAMESPACE --dry-run -o yaml | kubectl apply -f -
        kubectl label --overwrite namespace $KUBE_NAMESPACE istio-injection=enabled
        sed -i "s/APP_NAME/$APP_NAME/g;s/PROJECT_NAME/$PROJECT_NAME/g;s/APP_VERSION/$APP_VERSION/g;s/APP_BUILD/$APP_BUILD/g;s~DOCKER_IMAGE~${DOCKER_IMAGE}~g" k8s/$APP_ENVIRONMENT/crd-spec.yaml
        kubectl apply -f k8s/$APP_ENVIRONMENT/crd-spec.yaml --namespace $KUBE_NAMESPACE
        ;;

    "backend")
        kubectl version
        kubectl create namespace $KUBE_NAMESPACE --dry-run -o yaml | kubectl apply -f -
        kubectl label --overwrite namespace $KUBE_NAMESPACE istio-injection=enabled
        kubectl create secret generic app-sa-key --from-file /tmp/app-sa-key.json --namespace $KUBE_NAMESPACE --dry-run -o yaml | kubectl apply -f -
        sops --kms $SOPS_KMS -d -i k8s/$APP_ENVIRONMENT/secrets.yaml
        kubectl apply -f k8s/$APP_ENVIRONMENT/secrets.yaml --namespace $KUBE_NAMESPACE
        sed -i "s/APP_NAME/$APP_NAME/g;s/PROJECT_NAME/$PROJECT_NAME/g;s/APP_VERSION/$APP_VERSION/g;s/APP_BUILD/$APP_BUILD/g;s~DOCKER_IMAGE~${DOCKER_IMAGE}~g" k8s/$APP_ENVIRONMENT/crd-spec.yaml
        kubectl apply -f k8s/$APP_ENVIRONMENT/crd-spec.yaml --namespace $KUBE_NAMESPACE
        kubectl rollout status deployment $APP_NAME --namespace $KUBE_NAMESPACE --watch --timeout=5m
        ;;
    *)
        echo "deploy command require exactly one argument"
        exit 1
esac

